const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');

const router = express.Router();
const routes = require('./routes')(router);
const app = express();

const PORT = process.env.PORT || '8080';
const HOST = process.env.HOST || 'http://localhost';

app.use(logger('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', routes);

// error-handler settings
require('./config/error-handler')(app);
app.listen(PORT, () => console.log(`Server is ready at: ${HOST}:${PORT}`));
