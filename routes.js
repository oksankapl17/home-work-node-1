const { createWriteStream, readdirSync, readFile, stat, unlink } = require('fs');
const { promisify } = require('util');
const readFileAsync = promisify(readFile);
const statFileAsync = promisify(stat);
const removeFileAsync = promisify(unlink);

module.exports = (app) => {
  const UPLOADS_FOLDER = './uploads';
  app.post('/files', (req, res) => {
    const { content, filename } = req.body;
    if (!content) {
      return res.status(400).send({ message: "Please specify 'content' parameter" });
    }
    if (!filename) {
      return res.status(400).send({ message: "Please specify 'filename' parameter" });
    }
    const writeStream = createWriteStream(`${UPLOADS_FOLDER}/${filename}`);
    writeStream.write(content);
    writeStream.on('error', (err) => {
      res.send(err);
    });
    writeStream.on('finish', () => {
      res.send({ message: 'File created successfully' });
    });
    writeStream.end();
  });

  app.get('/files', (req, res) => {
    try {
      const files = readdirSync(UPLOADS_FOLDER).filter((filename) => !filename.startsWith('.'));
      return res.send({ message: 'Success', files });
    } catch (e) {
      res.send({ message: 'Client error' });
    }
  });

  app.get('/files/:filename', async (req, res) => {
    const { filename } = req.params;
    if (!filename) {
      return res.status(400).send({ message: `No file with '${filename}' filename found` });
    }
    try {
      const path = `${UPLOADS_FOLDER}/${filename}`;
      const file = await readFileAsync(path);
      const { birthtime } = await statFileAsync(path);
      return res.send({
        message: 'Success',
        filename,
        content: file.toString(),
        extension: filename.split('.').pop(),
        uploadedDate: birthtime
      });
    } catch (e) {
      return res.status(400).send({ message: `No file with '${filename}' filename found` });
    }
  });

  app.delete('/files/:filename', async (req, res) => {
    const { filename } = req.params;
    if (!filename) {
      return res.send({ message: "Please specify 'filename' parameter" });
    }
    try {
      const path = `${UPLOADS_FOLDER}/${filename}`;
      await removeFileAsync(path);
      return res.send({ message: 'File deleted successfully' });
    } catch (e) {
      return res.send({ message: `No file with '${filename}' filename found` });
    }
  });
  return app;
};
